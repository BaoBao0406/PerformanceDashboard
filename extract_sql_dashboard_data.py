#! python3
# extract_sql_dashboard_data.py - query sql data from sql server for Performance dashboard

import pyodbc
from datetime import date, datetime
import pandas as pd
from dateutil.relativedelta import relativedelta



def sql_user_data(conn):
    # FDC User ID and Name list
    user = pd.read_sql('SELECT DISTINCT(Id), Name \
                        FROM dbo.[User]', conn)
    user = user.set_index('Id')['Name'].to_dict()
    
    return user


# query Date Definite data
def sql_def_data(conn, user, start_date, end_date):
    date_def_df = pd.read_sql("SELECT BK.OwnerId, BK.nihrm__Property__c, ac.Name, ac.BillingCountry, ag.Name, BK.Name, FORMAT(BK.nihrm__ArrivalDate__c, 'MM/dd/yyyy') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'MM/dd/yyyy') AS DepartureDate, \
                                     BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, \
                                     BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__BookingStatus__c, FORMAT(BK.nihrm__LastStatusDate__c, 'MM/dd/yyyy') AS LastStatusDate, \
                                     FORMAT(BK.nihrm__BookedDate__c, 'MM/dd/yyyy') AS BookedDate, BK.End_User_Region__c, BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__DateDefinite__c, 'MM/dd/yyyy') AS DateDefinite, BK.Date_Definite_Month__c, BK.Date_Definite_Year__c \
                              FROM dbo.nihrm__Booking__c AS BK \
                                  LEFT JOIN dbo.Account AS ac \
                                      ON BK.nihrm__Account__c = ac.Id \
                                      LEFT JOIN dbo.Account AS ag \
                                      ON BK.nihrm__Agency__c = ag.Id \
                              WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                  (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c = 'Definite') AND \
                                  (BK.nihrm__DateDefinite__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "'))", conn)
    date_def_df.columns = ['Owner Name', 'Property', 'Account', 'Company Country', 'Agency', 'Booking: Booking Post As', 'Arrival', 'Departure', 
                          'Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Status',
                          'Last Status Date', 'Booked', 'End User Region', 'End User SIC', 'Booking Type', 'Booking ID#', 'DateDefinite', 'Date Definite Month', 'Date Definite Year']
    date_def_df['Owner Name'].replace(user, inplace=True)
    
    return date_def_df


# query Arrival Date data
def sql_arrival_data(conn, user, start_date, end_date):
    arrival_date_df = pd.read_sql("SELECT BK.OwnerId, BK.nihrm__Property__c, ac.Name, ac.BillingCountry, ag.Name, BK.Name, FORMAT(BK.nihrm__ArrivalDate__c, 'MM/dd/yyyy') AS ArrivalDate, BK.VCL_Arrival_Year__c, BK.VCL_Arrival_Month__c, FORMAT(BK.nihrm__DepartureDate__c, 'MM/dd/yyyy') AS DepartureDate, \
                                        BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__CurrentBlendedRevenueTotal__c, BK.nihrm__BookingStatus__c, FORMAT(BK.nihrm__LastStatusDate__c, 'MM/dd/yyyy') AS LastStatusDate, \
                                        FORMAT(BK.nihrm__BookedDate__c, 'MM/dd/yyyy') AS BookedDate, BK.Booked_Year__c, BK.Booked_Month__c, BK.End_User_Region__c, BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__DateDefinite__c, 'MM/dd/yyyy') AS DateDefinite, \
                                        BK.Date_Definite_Month__c, BK.Date_Definite_Year__c,  FORMAT(BK.LastActivityDate, 'MM/dd/yyyy') AS LastActivityDate \
                                    FROM dbo.nihrm__Booking__c AS BK \
                                    LEFT JOIN dbo.Account AS ac \
                                        ON BK.nihrm__Account__c = ac.Id \
                                    LEFT JOIN dbo.Account AS ag \
                                        ON BK.nihrm__Agency__c = ag.Id \
                                    WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                        (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND \
                                        (BK.nihrm__ArrivalDate__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "'))", conn)
    arrival_date_df.columns = ['Owner Name', 'Property', 'Account', 'Company Country', 'Agency', 'Booking: Booking Post As', 'Arrival', 'Arrival Year', 'Arrival Month', 'Departure', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                               'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended Total Revenue', 'Status', 'Last Status Date', 'Booked', 'Booked Year', 'Booked Month', 'End User Region', 'End User SIC', 'Booking Type', 'Booking ID#', 'DateDefinite', 
                               'Date Definite Month', 'Date Definite Year', 'LastActivityDate']
    arrival_date_df['Owner Name'].replace(user, inplace=True)
    arrival_date_df['Days before Arrive'] = ((pd.to_datetime(arrival_date_df['Arrival']) - pd.Timestamp.now().normalize()).dt.days).astype(int)
    arrival_date_df.sort_values(by=['Days before Arrive'], inplace=True)

    return arrival_date_df


# query Inquiry data
def sql_inquiry_data(conn, user):
    inquiry_df = pd.read_sql("SELECT inq.OwnerId, inq.nihrm__Property__c, inq.nihrm__Account__c, inq.nihrm__Company__c, inq.Name, inq.nihrm__ArrivalDate__c, inq.nihrm__Guests__c, inq.nihrm__TotalRooms__c, inq.nihrm__Status__c \
                           FROM dbo.nihrm__Inquiry__c AS inq", conn)
    inquiry_df.columns = ['Owner Name', 'Property', 'Account', 'Company', 'Name', 'Arrival', 'Guests', 'Total Rooms', 'Status']
    inquiry_df['Owner Name'].replace(user, inplace=True)
    
    return inquiry_df


# query ml booking data
def sql_ml_bk_data(conn, user):
    BK_ml_data = pd.read_sql("SELECT BK.Id, BK.Booking_ID_Number__c, FORMAT(BK.nihrm__ArrivalDate__c, 'yyyy-MM-dd') AS ArrivalDate, FORMAT(BK.nihrm__DepartureDate__c, 'yyyy-MM-dd') AS DepartureDate, BK.nihrm__CommissionPercentage__c, BK.Percentage_of_Attrition__c, BK.nihrm__Property__c, BK.nihrm__FoodBeverageMinimum__c, ac.Name AS ACName, ag.Name AS AGName, BK.End_User_Region__c, \
                                    BK.End_User_SIC__c, BK.nihrm__BookingTypeName__c, BK.RSO_Manager__c, BK.Non_Compete_Clause__c, ac.nihrm__RegionName__c, ac.Industry, BK.nihrm__CurrentBlendedRoomnightsTotal__c, BK.nihrm__BlendedGuestroomRevenueTotal__c, BK.VCL_Blended_F_B_Revenue__c, BK.nihrm__CurrentBlendedEventRevenue7__c, BK.nihrm__CurrentBlendedEventRevenue4__c, \
                                    BK.nihrm__BookingMarketSegmentName__c, BK.Promotion__c, BK.nihrm__CurrentBlendedADR__c, BK.nihrm__PeakRoomnightsBlocked__c, FORMAT(BK.nihrm__BookedDate__c, 'yyyy-MM-dd') AS BookedDate, FORMAT(BK.nihrm__LastStatusDate__c, 'yyyy-MM-dd') AS LastStatusDate \
                             FROM dbo.nihrm__Booking__c AS BK \
                                 LEFT JOIN dbo.Account AS ac \
                                     ON BK.nihrm__Account__c = ac.Id \
                                 LEFT JOIN dbo.Account AS ag \
                                     ON BK.nihrm__Agency__c = ag.Id \
                             WHERE (BK.nihrm__BookingTypeName__c NOT IN ('ALT Alternative', 'CN Concert', 'IN Internal')) AND \
                                 (BK.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (BK.nihrm__BookingStatus__c IN ('Tentative', 'Prospect'))", conn)
    BK_ml_data['RSO_Manager__c'].replace(user, inplace=True)
    BK_ml_data.columns = ['Id', 'BK_no', 'ArrivalDate', 'DepartureDate', 'Commission', 'Attrition', 'Property', 'F&B Minimum', 'Account', 'Agency', 'End User Region',
                      'End User SIC', 'Booking Type', 'RSO Manager', 'Non-compete', 'Account: Region', 'Account: Industry', 'Blended Roomnights', 'Blended Guestroom Revenue Total',
                      'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                      'BookedDate', 'LastStatusDate']
    
    return BK_ml_data


# query ml event data
def sql_ml_event_data(conn, start_date, end_date):
    Event_ml_data = pd.read_sql("SELECT ET.nihrm__Booking__c, ET.nihrm__Property__c, MAX(ET.nihrm__AgreedAttendance__c) \
                                FROM dbo.nihrm__BookingEvent__c AS ET \
                                WHERE (ET.nihrm__Property__c NOT IN ('Sands Macao Hotel')) AND (ET.nihrm__StartDate__c BETWEEN CONVERT(datetime, '" + start_date + "') AND CONVERT(datetime, '" + end_date + "')) \
                                GROUP BY ET.nihrm__Booking__c, ET.nihrm__Property__c", conn)
    Event_ml_data.columns = ['Id', 'property', 'Attendance']
    Event_ml_data = Event_ml_data[['Id', 'Attendance']]
    
    return Event_ml_data
    

# main function
def extract_sql_dashboard_data():
    
    # Set date range
    now = datetime.now()
    start_year = now - relativedelta(years=3)
    start_date = str(start_year.year) + '-01-01'
    # Definite date
    end_date_def = str(now.year) + '-12-31'
    # Arrival date
    end_year_arrival = now + relativedelta(years=5)
    end_date_arrival = str(end_year_arrival.year) + '-12-31'
    
    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=VOPPSCLDBN01\VOPPSCLDBI01;'
                          'Database=SalesForce;'
                          'Trusted_Connection=yes;')
    
    user = sql_user_data(conn)
    
    date_def_df = sql_def_data(conn, user, start_date, end_date_def)
    
    arrival_date_df = sql_arrival_data(conn, user, start_date, end_date_arrival)
    
    inquiry_df = sql_inquiry_data(conn, user)
    
    BK_ml_data = sql_ml_bk_data(conn, user)
    
    Event_ml_data = sql_ml_event_data(conn, start_date, end_date_arrival)
    
    BK_ml_data = BK_ml_data.join(Event_ml_data.set_index('Id'), on='Id')
    
    return date_def_df, arrival_date_df, inquiry_df, BK_ml_data

