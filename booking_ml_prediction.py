#! python3
# booking_ml_prediction.py - run ml prediction for materization and decision day

import pickle, re, os
import pandas as pd
import numpy as np


# ML prediction data preprocessing
def ml_prediction_data_preprocess(data):
    BK_ml_data = data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights',
                            'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance',
                            'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 'ArrivalDate', 'DepartureDate', 
                            'BookedDate', 'LastStatusDate']]
    # calculate Inhouse day (Departure - Arrival)    
    BK_ml_data['Inhouse day'] = (pd.to_datetime(BK_ml_data['DepartureDate']).dt.date - pd.to_datetime(BK_ml_data['ArrivalDate']).dt.date).dt.days
    # calculate Lead day (Arrival - Booked) 
    BK_ml_data['Lead day'] = (pd.to_datetime(BK_ml_data['ArrivalDate']).dt.date - pd.to_datetime(BK_ml_data['BookedDate']).dt.date).dt.days
    # calculate Decision day (Last Status date - Booked) 
    BK_ml_data['Decision day'] = (pd.to_datetime(BK_ml_data['LastStatusDate']).dt.date - pd.to_datetime(BK_ml_data['BookedDate']).dt.date).dt.days
    # booking info Arrival Month
    BK_ml_data['Arrival Month'] = pd.DatetimeIndex(BK_ml_data['ArrivalDate']).month
    BK_ml_data['Arrival Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Arrival Month']/12)
    # booking info Booked Month
    BK_ml_data['Booked Month'] = pd.DatetimeIndex(BK_ml_data['BookedDate']).month
    BK_ml_data['Booked Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Booked Month']/12)
    # booking info Last Status Month
    BK_ml_data['Last Status Month'] = pd.DatetimeIndex(BK_ml_data['LastStatusDate']).month
    BK_ml_data['Last Status Month_sin'] = np.sin(2 * np.pi * BK_ml_data['Last Status Month']/12)
    # convert text column to boolean
    BK_ml_data['Agency'] = BK_ml_data['Agency'].apply(lambda x: 0 if x is np.nan else 1)
    BK_ml_data['RSO Manager'] = BK_ml_data['RSO Manager'].apply(lambda x: 0 if x is np.nan else 1)
    BK_ml_data['Promotion'] = BK_ml_data['Promotion'].apply(lambda x: 0 if x is np.nan else 1)

    return BK_ml_data


# ML prediction - materization percent
def ml_prediction_materization(data, Transformer, Model):
    data = data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                     'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                     'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin', 'Booked Month_sin', 'Last Status Month_sin']]
    columns_to_standarize_percent = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                             'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_percent = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    column_normal_percent = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Decision day', 'Arrival Month_sin',
                     'Booked Month_sin', 'Last Status Month_sin']
    
    column_name_percent = columns_to_standarize_percent + columns_to_percent + column_normal_percent
    
    BK_ml_percent = Transformer.transform(data)
    BK_ml_percent = pd.DataFrame(BK_ml_percent, columns=column_name_percent)
    BK_ml_percent = BK_ml_percent.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    BK_ml_percent = BK_ml_percent.astype(float)
    y_pred_percent = Model.predict_proba(BK_ml_percent)
    # prediction materization percent
    y_pred_percent = pd.DataFrame(y_pred_percent, columns = ['TD %', 'D %'])
    y_pred_percent.reset_index(drop=True, inplace=True)

    return y_pred_percent


# ML prediction - decision day
def ml_prediction_decision_day(data, Transformer, Model):
    
    data = data[['Property', 'Account: Region', 'Account: Industry', 'Agency', 'End User Region', 'End User SIC', 'Booking Type', 'Blended Roomnights', 'Blended Guestroom Revenue Total', 
                                 'Blended F&B Revenue', 'Blended Rental Revenue', 'Blended AV Revenue', 'Attendance', 'RSO Manager', 'Market Segment', 'Promotion', 'Blended ADR', 'Peak Roomnights Blocked', 
                                 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Booked Month_sin']]
    columns_to_standarize_decision = ['Blended Roomnights', 'Blended Guestroom Revenue Total', 'Blended F&B Revenue', 'Blended Rental Revenue',
                             'Blended AV Revenue', 'Attendance', 'Blended ADR', 'Peak Roomnights Blocked']
    columns_to_te_decision = ['Property', 'Account: Region', 'Account: Industry', 'End User Region', 'End User SIC', 'Booking Type', 'Market Segment']
    column_normal_decision = ['Agency', 'RSO Manager', 'Promotion', 'Inhouse day', 'Lead day', 'Arrival Month_sin', 'Booked Month_sin']
    
    column_name_decision = columns_to_standarize_decision + columns_to_te_decision + column_normal_decision
    
    BK_ml_decision = Transformer.transform(data)
    BK_ml_decision = pd.DataFrame(BK_ml_decision, columns=column_name_decision)
    BK_ml_decision = BK_ml_decision.rename(columns = lambda x:re.sub('[^A-Za-z0-9_]+', '', x))
    BK_ml_decision = BK_ml_decision.astype(float)
    y_pred_decision = Model.predict_proba(BK_ml_decision)
    # prediction decision day
    y_pred_decision = pd.DataFrame(y_pred_decision, columns = ['Decision period'])
    y_pred_decision.reset_index(drop=True, inplace=True)
    
    return y_pred_decision


# main function
def booking_ml_prediction(data):
    
    # Load Transformer and prediction model - materization percent
    path = os.path.abspath(os.getcwd()) + '\\pkl_file\\'
    
    Transformer_percent = open(path + 'Materization_percent_tf.pkl', 'rb')
    Transformer_percent = pickle.load(Transformer_percent)
    
    Model_percent = open(path + 'Materization_percent_ml.pkl', 'rb')
    Model_percent = pickle.load(Model_percent)
    
    # Load Transformer and prediction model - decision day
    Transformer_decision = open(path + 'decision_day_tf.pkl', 'rb')
    Transformer_decision = pickle.load(Transformer_decision)
    
    Model_decision = open(path + 'decision_day_ml.pkl', 'rb')
    Model_decision = pickle.load(Model_decision)
    
    preprocess_data = ml_prediction_data_preprocess(data)
    
    y_pred_percent = ml_prediction_materization(preprocess_data, Transformer_percent, Model_percent)
    
    y_pred_decision = ml_prediction_decision_day(preprocess_data, Transformer_decision, Model_decision)
    
    # Join decision day and materization percent
    BK_ml_pred_data = data[['BK_no']]
    BK_ml_pred_data.reset_index(drop=True, inplace=True)
    BK_ml_pred_data = pd.concat([BK_ml_pred_data, y_pred_percent, y_pred_decision], axis=1)
    
    return BK_ml_pred_data
